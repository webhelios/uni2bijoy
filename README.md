# Php class for converting Bengali Fonts from Unicode(utf-8) to Bijoy(ASCII)  #

##  How to use  ##
1. Download uniToBijoy.php file
2. include it on your php file, create a obj and class the convert function

```
#!php

<?php
require 'uniToBijoy.php';
$uni_text = 'আমার সোনার বাংলা,আমি তোমায় ভালবাসি';
$obj = new uniToBijoy();
$bijoy_text = $obj->convert($uni_text);
?>
```
## Demo ##
[Check the Demo Here](http://localhost/Uni2Bijoy/)