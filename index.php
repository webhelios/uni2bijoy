<!DOCTYPE html>
<html lang="utf-8">
	<head>
		<title>Unicode to Bijoy(ASCII) converter</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style type="text/css">
			.bijoy-text {
		    font-family: 'SutonnyMJ', sans-serif;
			}
		</style>
	</head>
	<body>
		<?php
		require 'uniToBijoy.php';
		$uni_text = 'আমার সোনার বাংলা,আমি তোমায় ভালবাসি';
		$obj = new uniToBijoy();
		$bijoy_text = $obj->convert($uni_text);
		?>
		<h2>Unicode text</h2>
		<div class="uni-text"><?php echo $uni_text;?></div>
		<h2>Bijoy(ASCII) text</h2>
		<div class="bijoy-text"><?php echo $bijoy_text;?></div>
	</body>
</html>

